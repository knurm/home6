import java.util.*;

/**
 * Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

    /**
     * Main method.
     */
    public static void main(String[] args) {
        GraphTask a = new GraphTask();
        a.run();
    }

    /**
     * Actual main method to run examples and everything.
     */
    public void run() {
        Graph g = new Graph("G", null);
        Map<String, Integer> connects = new HashMap<>();
        connects.put("a1", 10);
        connects.put("a2", 10);
        connects.put("a3", 5);
        connects.put("a4", 900);
        connects.put("a5", 15);
        connects.put("a6", 20);
        connects.put("a7", 5);
        connects.put("a8", 15);
        Set<Flight> flights = new HashSet<>();
        flights.add(new Flight("a1", "a2", 20, 100));
        flights.add(new Flight("a2", "a3", 120, 200));
        flights.add(new Flight("a1", "a4", 120, 200));
        flights.add(new Flight("a4", "a5", 200, 400));
        flights.add(new Flight("a3", "a5", 300, 500));
        flights.add(new Flight("a3", "a8", 110, 210));
        flights.add(new Flight("a8", "a6", 250, 400));
        flights.add(new Flight("a5", "a6", 520, 600));
        flights.add(new Flight("a5", "a8", 450, 495));
        flights.add(new Flight("a4", "a7", 250, 340));
        flights.add(new Flight("a1", "a8", 5, 100));
        flights.add(new Flight("a6", "a7", 430, 490));
        flights.add(new Flight("a2", "a8", 110, 150));
        flights.add(new Flight("a8", "a7", 200, 370));
        g.createFromFlightSet(connects, flights);
        System.out.println(g.flightSequence("a1", "a5", 20));
        System.out.println(g.flightSequence("a3", "a6", 160));
        System.out.println(g.flightSequence("a1", "a7", 30));
        System.out.println(g.flightSequence("a3", "a6", 140));
        System.out.println(g.flightSequence("a1", "a7", 120));
    }

    /**
     * Vertex represents one vertex in the graph.
     */
    class Vertex {

        private String id;
        private Vertex next;
        private Arc first;
        private int info = 0;
        private int connectTime;
        // You can add more fields, if needed

        Vertex(String s, int connect, Vertex v, Arc e) {
            id = s;
            connectTime = connect;
            next = v;
            first = e;
        }

        Vertex(String s, int connect) {
            this(s, connect, null, null);
        }

        @Override
        public String toString() {
            return id;
        }
    }


    /**
     * Arc represents one arrow in the graph. Two-directional edges are
     * represented by two Arc objects (for both directions).
     */
    class Arc {

        private String id;
        private Vertex target;
        private int arrivalTime;
        private int depTime;
        private Arc next;
        private int info = 0;
        // You can add more fields, if needed

        Arc(String s, int dep, int arr, Vertex v, Arc a) {
            id = s;
            depTime = dep;
            arrivalTime = arr;
            target = v;
            next = a;
        }

        Arc(String s) {
            this(s, 0, 0, null, null);
        }

        Arc(String s, int dep, int arr) {
            this(s, dep, arr, null, null);
        }

        @Override
        public String toString() {
            return id;
        }
    }


    class Graph {

        private String id;
        private Vertex first;
        private int info = 0;
        // You can add more fields, if needed

        Graph(String s, Vertex v) {
            id = s;
            first = v;
        }

        Graph(String s) {
            this(s, null);
        }

        @Override
        public String toString() {
            String nl = System.getProperty("line.separator");
            StringBuffer sb = new StringBuffer(nl);
            sb.append(id);
            sb.append(nl);
            Vertex v = first;
            while (v != null) {
                sb.append(v.toString());
                sb.append(" -->");
                Arc a = v.first;
                while (a != null) {
                    sb.append(" ");
                    sb.append(a.toString());
                    sb.append(" (");
                    sb.append(v.toString());
                    sb.append("->");
                    sb.append(a.target.toString());
                    sb.append(" ");
                    sb.append(a.arrivalTime);
                    sb.append(")");
                    a = a.next;
                }
                sb.append(nl);
                v = v.next;
            }
            return sb.toString();
        }

        public Vertex createVertex(String vid, int connectAt) {
            Vertex res = new Vertex(vid, connectAt);
            res.next = first;
            first = res;
            return res;
        }

        public Arc createArc(String aid, Vertex from, Vertex to, int dep, int arr) {
            Arc res = new Arc(aid, dep, arr);
            res.next = from.first;
            from.first = res;
            res.target = to;
            return res;
        }

        public Arc createArc(String aid, Vertex from, Vertex to) {
            return createArc(aid, from, to, 0, 0);
        }

        /**
         * Create a connected undirected random tree with n vertices.
         * Each new vertex is connected to some random existing vertex.
         *
         * @param n number of vertices added to this graph
         */
        public void createRandomTree(int n) {
            if (n <= 0)
                return;
            Vertex[] varray = new Vertex[n];
            for (int i = 0; i < n; i++) {
                varray[i] = createVertex("v" + String.valueOf(n - i), 0);
                if (i > 0) {
                    int vnr = (int) (Math.random() * i);
                    createArc("a" + varray[vnr].toString() + "_"
                            + varray[i].toString(), varray[vnr], varray[i]);
                    createArc("a" + varray[i].toString() + "_"
                            + varray[vnr].toString(), varray[i], varray[vnr]);
                } else {
                }
            }
        }

        /**
         * Create an adjacency matrix of this graph.
         * Side effect: corrupts info fields in the graph
         *
         * @return adjacency matrix
         */
        public int[][] createAdjMatrix() {
            info = 0;
            Vertex v = first;
            while (v != null) {
                v.info = info++;
                v = v.next;
            }
            int[][] res = new int[info][info];
            v = first;
            while (v != null) {
                int i = v.info;
                Arc a = v.first;
                while (a != null) {
                    int j = a.target.info;
                    res[i][j]++;
                    a = a.next;
                }
                v = v.next;
            }
            return res;
        }

        /**
         * Create a connected simple (undirected, no loops, no multiple
         * arcs) random graph with n vertices and m edges.
         *
         * @param n number of vertices
         * @param m number of edges
         */
        public void createRandomSimpleGraph(int n, int m) {
            if (n <= 0)
                return;
            if (n > 2500)
                throw new IllegalArgumentException("Too many vertices: " + n);
            if (m < n - 1 || m > n * (n - 1) / 2)
                throw new IllegalArgumentException
                        ("Impossible number of edges: " + m);
            first = null;
            createRandomTree(n);       // n-1 edges created here
            Vertex[] vert = new Vertex[n];
            Vertex v = first;
            int c = 0;
            while (v != null) {
                vert[c++] = v;
                v = v.next;
            }
            int[][] connected = createAdjMatrix();
            int edgeCount = m - n + 1;  // remaining edges
            while (edgeCount > 0) {
                int i = (int) (Math.random() * n);  // random source
                int j = (int) (Math.random() * n);  // random target
                if (i == j)
                    continue;  // no loops
                if (connected[i][j] != 0 || connected[j][i] != 0)
                    continue;  // no multiple edges
                Vertex vi = vert[i];
                Vertex vj = vert[j];
                createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
                connected[i][j] = 1;
                createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
                connected[j][i] = 1;
                edgeCount--;  // a new edge happily created
            }
        }

        /**
         * Create a connected simple graph with connects  as vertices and
         * flights  as edges.
         *
         * @param connects Map of airports and connecting times
         * @param flights  Set of departing airport, arriving airport,
         *                 departure time and arriving time
         */
        public void createFromFlightSet(Map<String, Integer> connects, Set<Flight> flights) {
            int n = connects.size();
            if (n <= 0)
                return;
            if (n > 2500)
                throw new IllegalArgumentException("Too many vertices: " + n);
            Map<String, Vertex> vertices = new HashMap<>();
            for (String a : connects.keySet()) {
                vertices.put(a, createVertex(a, connects.get(a)));
            }
            for (Flight f : flights) {
                Vertex vi = vertices.get(f.start);
                Vertex vj = vertices.get(f.end);
                createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj, f.depTime, f.arrival);
            }
        }

        /**
         * Get vertex by its id.
         *
         * @param vid Vertex' id
         */
        public Vertex getVertexById(String vid) {
            Vertex v = first;
            while (!v.id.equals(vid)) {
                v = v.next;
            }
            return v;
        }

        /**
         * Get flights departing from specific airport.
         *
         * @param vid  Vertex' id
         * @param time desirable departing time
         */
        public List<Arc> flightsDepartingFrom(String vid, int time) {
            List<Arc> departing = new ArrayList<>();
            Vertex v = getVertexById(vid);
            Arc a = v.first;
            while (a != null) {
                if (a.depTime >= time) {
                    departing.add(a);
                }
                a = a.next;
            }
            return departing;
        }

        /**
         * Extend sequence.
         *
         * @param sequence Vertex' id
         */
        public List<Sequence> extendSequence(Sequence sequence) {
            List<Sequence> extended = new ArrayList<>();
            int time = sequence.time;
            time += sequence.lastFlight == null ? 0 : sequence.stoppedAt.connectTime;
            List<Arc> accessible = flightsDepartingFrom(sequence.stoppedAt.id, time);
            for (Arc a : accessible) {
                extended.add(sequence.extend(a));
            }
            return extended;
        }

        /**
         * Get the flight sequence with the shortest path.
         *
         * @param departing departing airport
         * @param arriving  arriving airport
         * @param time      desirable departure time
         */
        public String flightSequence(String departing, String arriving, int time) {
            Sequence initial = new Sequence(time, getVertexById(departing));
            // This ensures that we always pick the best sequence at the moment
            PriorityQueue<Sequence> sequences = new PriorityQueue<>(10, new Comparator<Sequence>() {
                @Override
                public int compare(Sequence o1, Sequence o2) {
                    return o1.time - o2.time;
                }
            });
            Sequence current = initial;
            while (current != null && !current.stoppedAt.id.equals(arriving)) {
                sequences.addAll(extendSequence(current));
                current = sequences.poll();
            }
            return "Flight sequence to follow: " + current.sequenceSoFar + " Arrival time is: " + current.time;
        }
    }

    /**
     * Flight holds flights departure airport,
     * arriving airport, departing time and arriving time
     */
    class Flight {
        String start;
        String end;
        int depTime;
        int arrival;

        Flight(String start, String end, int depTime, int arrival) {
            this.start = start;
            this.end = end;
            this.depTime = depTime;
            this.arrival = arrival;
        }
    }

    /**
     * Sequence holds the current airports id,
     * last flight, running time and Vertex
     * where the last stop was.
     */
    class Sequence {
        String sequenceSoFar = "";
        Arc lastFlight;
        int time;
        Vertex stoppedAt;

        public Sequence(int time, Vertex stopped) {
            this.time = time;
            this.stoppedAt = stopped;
            sequenceSoFar = stopped.id;
        }

        public Sequence(String sequenceSoFar, Arc lastFlight, int time, Vertex stoppedAt) {
            this.sequenceSoFar = sequenceSoFar;
            this.lastFlight = lastFlight;
            this.time = time;
            this.stoppedAt = stoppedAt;
        }

        public Sequence extend(Arc a) {
            String newSeq = sequenceSoFar + " -> " + a.target.id;
            return new Sequence(newSeq, a, a.arrivalTime, a.target);
        }
    }

}